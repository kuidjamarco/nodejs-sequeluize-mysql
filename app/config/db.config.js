module.exports = {
    HOST: "localhost",
    USER: "Marco",
    PASSWORD: "MySQL8.0.25",
    DB: "node_tutorials",
    dialect: "mysql",
    pool: {
      max: 5,
      min: 0,
      acquire: 30000,
      idle: 10000
    }
};